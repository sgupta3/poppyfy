//
//  PPReviewImageViewController.h
//  Poppyfy
//
//  Created by Sahil Gupta on 2015-08-03.
//  Copyright (c) 2015 Sahil Gupta. All rights reserved.
//

#import "ViewController.h"

@interface PPReviewImageViewController : ViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic) UIImage *image;

@end
