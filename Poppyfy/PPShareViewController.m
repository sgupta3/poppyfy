//
//  PPShareViewController.m
//  Poppyfy
//
//  Created by Sahil Gupta on 2015-08-03.
//  Copyright (c) 2015 Sahil Gupta. All rights reserved.
//

#import "PPShareViewController.h"
#import <ChameleonFramework/Chameleon.h>


@interface PPShareViewController ()
@property (weak, nonatomic) IBOutlet UIView *savePhotoView;

@end

@implementation PPShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageView.image = _image;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)savePhotoToCameraRoll:(id)sender {
    
    [self giveButtonVisualFeedback:self.savePhotoView];
    UIImageWriteToSavedPhotosAlbum(_image, nil, nil, nil);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Saved" message:@"Image has been saved to the camera roll" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    
    [alertView show];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
    });
}

- (IBAction)sharePhotoView:(id)sender {
    NSString *textToShare = @"#Poppyfy";
    UIImage *imageToShare = _image;
    NSArray *itemsToShare = @[textToShare, imageToShare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
   // activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //or whichever you don't need
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (void) giveButtonVisualFeedback:(UIView *)view {
    view.backgroundColor = [UIColor flatRedColor];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        view.backgroundColor = [UIColor flatRedColorDark];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
