//
//  AppDelegate.h
//  Poppyfy
//
//  Created by Sahil Gupta on 2015-08-03.
//  Copyright (c) 2015 Sahil Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

