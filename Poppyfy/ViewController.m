//
//  ViewController.m
//  Poppyfy
//
//  Created by Sahil Gupta on 2015-08-03.
//  Copyright (c) 2015 Sahil Gupta. All rights reserved.
//

#import "ViewController.h"
#import "PPReviewImageViewController.h"
#import <ChameleonFramework/Chameleon.h>


@interface ViewController () <UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *takePhotoView;
@property (weak, nonatomic) IBOutlet UIView *choosePhotoView;
@property (nonatomic) UIImage *selectedImage;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.takePhotoView.userInteractionEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)takePhotoPressed:(id)sender {
    [self giveButtonVisualFeedback:self.takePhotoView];
    NSLog(@"Take photo");
    
    if([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.allowsEditing = NO;
        controller.delegate = self;
        [self.navigationController presentViewController: controller animated: YES completion: nil];
    }

}

- (IBAction)chooseFromLibraryPressed:(id)sender {
    [self giveButtonVisualFeedback:self.choosePhotoView];
    NSLog(@"Choose from Library Pressed");
    
    if([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.allowsEditing = NO;
        controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
        controller.delegate = self;
        [self.navigationController presentViewController: controller animated: YES completion: nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        _selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        [self performSegueWithIdentifier:@"reviewImageSegue" sender:self];
    }];

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual:@"reviewImageSegue"]){
        PPReviewImageViewController *destinationController = (PPReviewImageViewController *) segue.destinationViewController;
        destinationController.image = _selectedImage;
    }
}



#pragma mark Helpers

- (void) giveButtonVisualFeedback:(UIView *)view {
    view.backgroundColor = [UIColor flatRedColor];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        view.backgroundColor = [UIColor flatRedColorDark];
    });
}






@end
