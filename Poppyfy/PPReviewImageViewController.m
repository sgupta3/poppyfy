//
//  PPReviewImageViewController.m
//  Poppyfy
//
//  Created by Sahil Gupta on 2015-08-03.
//  Copyright (c) 2015 Sahil Gupta. All rights reserved.
//

#import "PPReviewImageViewController.h"
#import "PPShareViewController.h"

@interface PPReviewImageViewController ()

@property (weak, nonatomic) IBOutlet UISlider *slider;
@end

@implementation PPReviewImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setRightNavigationItem];
    [self processImage:1];
    self.imageView.frame = CGRectMake(self.imageView.frame.origin.x,self.imageView.frame.origin.y,self.image.size.width,self.image.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)donePressed {
    [self performSegueWithIdentifier:@"shareSegue" sender:self];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqual:@"shareSegue"]) {
        PPShareViewController *destinationViewController = (PPShareViewController *) segue.destinationViewController;
        destinationViewController.image = self.imageView.image;
    }
}



#pragma mark Helpers

- (UIImage*) drawImage:(UIImage*) fgImage
               inImage:(UIImage*) bgImage
               atPoint:(CGPoint)  point
{
    UIGraphicsBeginImageContextWithOptions(bgImage.size, FALSE, 0.0);
    [bgImage drawInRect:CGRectMake( 0, 0, bgImage.size.width, bgImage.size.height)];
    [fgImage drawInRect:CGRectMake( point.x, point.y, fgImage.size.width, fgImage.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (void) setRightNavigationItem {
    UIImage *faceImage = [UIImage imageNamed:@"done"];
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    face.bounds = CGRectMake( 0, 0, 25, 25 );//set bound as per you want
    [face addTarget:self action:@selector(donePressed) forControlEvents:UIControlEventTouchUpInside];
    [face setImage:faceImage forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:face];
    self.navigationItem.rightBarButtonItem = backButton;
}

- (void) processImage: (float) number {
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        CGSize originalImageSize = CGSizeMake(self.image.size.width, self.image.size.height);
        CGSize poppyImageSize = CGSizeMake(originalImageSize.width * .3 * number, originalImageSize.width * .3 * number);
        UIImage *poppyImage = [self imageWithImage:[UIImage imageNamed:@"poppy"] scaledToSize:CGSizeMake(poppyImageSize.width , poppyImageSize.height)];
        CGPoint point = CGPointMake(self.image.size.width - poppyImageSize.width, self.image.size.height - poppyImageSize.height);
        UIImage *finalImage = [self drawImage:poppyImage inImage:self.image atPoint:point];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            self.imageView.image = finalImage;
            
        });
    });
    
}

- (IBAction)changePoppySize:(id)sender {
    [self processImage:self.slider.value];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
